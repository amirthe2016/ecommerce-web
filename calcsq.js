//named function

function calsq(a){
	return a+a;
}
console.log("calling called name function:"+calsq(3));

//anonymous function
var sqcal=function(m){
	return m*m;
}
console.log("called anonymous funtion:"+sqcal(6));

//new constructor
var cal=new Function("a","return a*a;");
console.log("calling constructor function:"+cal(7));

//self invoking function
(function(a){
	console.log("calling self invoking function:"+a*a);
	return a*a;
})(9);